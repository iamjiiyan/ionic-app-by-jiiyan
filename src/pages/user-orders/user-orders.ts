import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController, AlertController} from 'ionic-angular';
import {WoocommerceProvider} from '../../providers/woocommerce/woocommerce'
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';


@IonicPage({})
@Component({
  selector: 'page-user-orders',
  templateUrl: 'user-orders.html',
})
export class UserOrdersPage {


  WooCommerce: any;
  customerId: number;
  customerData: any = [];
  userInfo: any;
  customerOrders: any = [];
  featuredThumbnail: any;
  // products: any = [];
  // orders: any[];
  page: number;

  constructor(public alertCtrl: AlertController, private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    this.page = 1;
    this.customerData = {};

    this.WooCommerce = this.WP.init();

    let loading = this.loadingCtrl.create({
      content: `Loading orders...`,
    });

    loading.present();

    this.storage.get("userLoginInfo").then((userLoginInfo) => {

      this.userInfo = userLoginInfo.user;

      // let email = userLoginInfo.user.email;
      this.customerId = userLoginInfo.user.id;

      this.WooCommerce.getAsync("orders?customer="+this.customerId).then((data) => {
  
        this.customerOrders = JSON.parse(data.body).orders;
        console.log(this.customerOrders);

        loading.dismiss();
        // this.customerOrders.forEach((element,index) => {
        //   // let mediaRequest = element._links['wp:featuredmedia'][0].href;  
        //   let productId = element.line_items[0].product_id;
  
        //   this.WooCommerce.getAsync('products/' + productId).then((data) => {
        //     let productThumb = JSON.parse(data.body);
        //     element['featured_image'] = productThumb.images[0].src;
  
        //     this.customerOrders.push(element);
        //   });
        // });
        
       
        
  
      },(err)=>{
        console.log(err);
        this.alertCtrl.create({
          title: "Error while placing order.",
          message: err.message,
          buttons: [{
            text: "OK",
          }]
        }).present();
      })
    })
    // console.log(this.userInfo);

   

  }

  loadMoreOrders(event){
    console.log(event); 
    if(event == null)
    {
      this.page = 2;
      this.customerOrders = [];
    }
    else
      this.page++;

    this.WooCommerce.getAsync("orders?customer="+this.customerId+"&page=" + this.page).then( (data) => {
      this.customerOrders = this.customerOrders.concat(JSON.parse(data.body));
      console.log(JSON.parse(data.body));
      let loading = this.loadingCtrl.create({
        content: `Loading orders...`,
      });

      loading.present();

      event.complete();
      loading.dismiss();
     
     let response = JSON.parse(data.body);
     if(response.length < 10){
       event.enable(false);

       this.toastCtrl.create({
         message: "No more products!",
         duration: 5000
       }).present();
     }


    }, (err) => {
      console.log(err);
    });
  }

  openUserOrderDetailsPage(order){
    this.navCtrl.push('UserOrderDetailsPage', {"order": order});
  }

  openProductPage(product){
    this.navCtrl.setRoot('ProductDetailsPage', {"product": product});
  }

  backToHomepage(){
    this.navCtrl.setRoot('HomePage');
  }

  openCart(){
    this.modalCtrl.create(CartPage).present();
  }


  openSearchPage(){
    this.navCtrl.push('SearchPage')
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController,LoadingController } from 'ionic-angular';
// import  * as WC from 'woocommerce-api';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';
@IonicPage({})
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  newUser: any = {};
  billing_shipping_same: boolean;
  WooCommerce: any;

  constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    this.newUser.billing_address = {};
    this.newUser.shipping_address = {};
    this.newUser.shipping_address = false;

    this.WooCommerce = this.WP.init();
   
    // this.WooCommerce = WC({
    //   // This is Require to get data from woocommerces
    //   url: "https://ido-ido.co/mangbenta",
    //   consumerKey: "ck_80e53d5753dc9beea00445b174a66d625f3a375e",
    //   consumerSecret: "cs_861f550cea5ec6a9da2f073018a872f943c70d3f",
    //   // signature: 'Si5CqCqy0jEjO6UfjWS4%2FC8sR%2Babd7eaGQmMijTAFAc%3D',
    //   // queryStringAuth: 'oauth_consumer_key="ck_8b06eeee0c44f2050c741b965d357d618714b1ea",oauth_signature_method="HMAC-SHA256",oauth_timestamp="1531052724",oauth_nonce="ZJvb8W",oauth_version="1.0",oauth_signature="12aC1Ym5YiHj6svsjQFdMca0Ka6kcvDQoZoGviuNY0A%3D',
      
    //   // wpAPI: true,
    //   // version: 'wc/v3' 
    //   // url: "http://mangbenta.com",
    //   // consumerKey: "ck_d6d86dad86324938cb3424eb4bb8997b4dc0fa7c",
    //   // consumerSecret: "cs_884ed434c0b993f14244c4418421af18bc035bd4",
    //   // verifySsl: false,
    //   // wpAPI: true,
    //   // version: 1.0,
    //   // queryStringAuth: false
    // });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  setBillingToShipping(){
    this.billing_shipping_same = !this.billing_shipping_same;
  }


  checkEmail(){
    let validEmail = false;

    let reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let loading = this.loadingCtrl.create({
      content: `Validating Email...`,
    });

    loading.present();
    if(reg.test(this.newUser.email)){

      this.WooCommerce.getAsync('customers/email/' + this.newUser.email).then((data)=>{
      let res = JSON.parse(data.body);
      
      if(res.errors){
        validEmail = true;
        this.toastCtrl.create({
          message: "Conmgratulations, Email is good to go.",
          duration: 3000
        }).present();
      }else {
        validEmail = false;
        this.toastCtrl.create({
          message: "Email already registered. Please check.",
          showCloseButton: true,
        }).present();
      }
      loading.dismiss();

      console.log(validEmail);
      console.log(JSON.parse(data.body));

      });
    }else {
      validEmail = false;
      this.toastCtrl.create({
        message: "Invalid Email. Please Check",
        duration: 3000
      }).present();
      console.log(validEmail);
    }
  }

  signup(){
    
    let loading = this.loadingCtrl.create({
      content: `Signing up...`,
    });

    loading.present();

    let customerData = {
      customer : {}
    }

    customerData.customer = {
      "email": this.newUser.email,
      "first_name": this.newUser.first_name,
      "last_name": this.newUser.last_name,
      "username": this.newUser.username,
      "password": this.newUser.password,
      "billing_address": {
        "first_name": this.newUser.first_name,
        "last_name": this.newUser.last_name,
        "company": "",
        "address_1": this.newUser.billing_address.address_1,
        "address_2": this.newUser.billing_address.address_2,
        "city": this.newUser.billing_address.city,
        "state": this.newUser.billing_address.state,
        "postcode": this.newUser.billing_address.postcode,
        "country": this.newUser.billing_address.country,
        "email": this.newUser.email,
        "phone": this.newUser.billing_address.phone
      },
      "shipping_address": {
        "first_name": this.newUser.first_name,
        "last_name": this.newUser.last_name,
        "company": "",
        "address_1": this.newUser.shipping_address.address_1,
        "address_2": this.newUser.shipping_address.address_2,
        "city": this.newUser.shipping_address.city,
        "state": this.newUser.shipping_address.state,
        "postcode": this.newUser.shipping_address.postcode,
        "country": this.newUser.shipping_address.country
      }
    }

    if(this.billing_shipping_same){
      this.newUser.shipping_address = this.newUser.shipping_address;
    }

    this.WooCommerce.postAsync('customers', customerData).then( (data) => {

      let response = (JSON.parse(data.body));
      loading.dismiss();
      if(response.customer){
        this.alertCtrl.create({
          title: "Account Created",
          message: "Your account has been created successfully! Please login to proceed.",
          buttons: [{
            text: "Login",
            handler: ()=> {
              this.navCtrl.push('LoginPage');
            }
          }]
        }).present();
      } else if(response.errors){
        this.toastCtrl.create({
          message: response.errors[0].message,
          showCloseButton: true
        }).present();
      }

    })

  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
// import  * as WC from 'woocommerce-api';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';

@IonicPage({})
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  searchQuery: string = "";
  WooCommerce: any;
  products: any[] = [];
  page: number = 2;
    
  constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
      this.searchQuery = this.navParams.get("searchQuery");

      this.WooCommerce = this.WP.init();
      
      
  }


  loadMoreProducts(event){
    this.WooCommerce.getAsync("products?filter[q]="+ this.searchQuery+"&page="+this.page).then((searchData) => {
      this.products = this.products.concat(JSON.parse(searchData.body).products);
  
    if(JSON.parse(searchData.body).products.length < 10){
      event.enable(false);
      this.toastCtrl.create({
        message: "No more products!",
        duration: 5000
      }).present();
    }      

      event.complete();

      this.page ++;
    });
  }


  openProductPage(product){
    this.navCtrl.push('ProductDetailsPage', {"product": product});
  }

  onSearch(event){
    this.WooCommerce.getAsync("products?filter[q]="+ this.searchQuery).then((searchData) => {
      
      // let loading = this.loadingCtrl.create({
      //   spinner: 'hide',
      //   content: `<img src="../assets/images/icon.png"><h2>Seaching Products...</h2>`,
      //   });

      // loading.present();

      // setTimeout(() => {
      //   loading.dismiss();
      // }, 2000);

      this.products = JSON.parse(searchData.body).products;

      console.log(this.products);
    });
    console.log(this.navParams.get("searchQuery"));
  }
}
 
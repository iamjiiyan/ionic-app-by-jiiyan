import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { SignupPage } from '../signup/signup';
// import { LoginPage } from '../login/login';
import { CartPage } from '../cart/cart';
// import  * as WC from 'woocommerce-api';
// import { ProductsByCategoryPage } from '../products-by-category/products-by-category';
// import { PostPage } from '../post/post';
import { Storage } from '@ionic/storage';
import {WoocommerceProvider} from '../../providers/woocommerce/woocommerce'
// import { UserOrdersPage } from '../user-orders/user-orders';

@IonicPage({})

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  HomePage: any;
  WooCommerce: any;
  categories: any[];
  @ViewChild('content') childNavCtrl: NavController;
  loggedIn: boolean;
  user: any;
  
  constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public modalCtrl: ModalController) {
    this.HomePage = 'HomePage';
    this.categories = [];
    this.user = {};

    
    this.WooCommerce = this.WP.init();

    this.WooCommerce.getAsync("products/categories").then((data)=> {
      // console.log(JSON.parse(data.body).product_categories);

      let temp: any[] = JSON.parse(data.body).product_categories;

      for(let i=0; i < temp.length; i++){
        if(temp[i].count != 0){
          if(temp[i].parent==0){
            
            temp[i].subCategories = [];

          // if (temp[i].slug == "clothing") {
          //   temp[i].icon = "shirt";
          // }
          // if (temp[i].slug == "music") {
          //   temp[i].icon = "musical-notes";
          // }
          // if (temp[i].slug == "posters") {
          //   temp[i].icon = "images";
          // }

            
  
            this.categories.push(temp[i]);
          }
        }
      }

      // Group Subcat


      for (let i = 0; i < temp.length; i++){
        for (let j = 0; j < this.categories.length; j++){
          //console.log("Checking " + j + " " + i)
          if(this.categories[j].id == temp[i].parent){
            this.categories[j].subCategories.push(temp[i]);
          }
        }
      }


      // console.log(this.categories);

    },(err)=>{
      console.log(err);
    })
  }

  ionViewDidEnter() {
   
    this.storage.ready().then( () =>{
      this.storage.get("userLoginInfo").then( (userLoginInfo)=> {
        if(userLoginInfo != null){
          console.log("User logged In...");
          this.user = userLoginInfo.user;

          // console.log(this.user);

          this.loggedIn = true;
        }else {
          console.log("No user found...");
          this.user = {};
          this.loggedIn = false;
        }
      })
    })

  }

  openCategoryPage(category){
    console.log(category);
    this.childNavCtrl.setRoot('ProductsByCategoryPage', {"category": category});
  }

  openArchivePage(postType){
    // console.log(postType);
    this.childNavCtrl.push('PostPage', {"postType": postType});
  }

  openCustomerOrders(userId){
    // console.log(userId);
    this.childNavCtrl.setRoot('UserOrdersPage', {"userId": userId});
  }

  openPage(pageName:string){
    if(pageName == 'signup'){
      this.navCtrl.push('SignupPage');
    }
    if(pageName == 'login'){
      this.navCtrl.push('LoginPage');
    }
    if(pageName == 'logout'){
      this.storage.remove('userLoginInfo').then( () => {
        this.user = {};
        this.loggedIn = false;
      })
      this.childNavCtrl.setRoot('HomePage');
    }
    if(pageName == 'cart'){
      let modal = this.modalCtrl.create(CartPage);
      modal.present();
    }


  }
}

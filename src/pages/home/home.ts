import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, ToastController, ModalController, IonicPage, LoadingController} from 'ionic-angular';
// import  * as WC from 'woocommerce-api';
// import { ProductDetailsPage } from '../product-details/product-details';
import { CartPage } from '../cart/cart';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';

@IonicPage({})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  WooCommerce: any;
  products: any[];
  moreProducts: any[];    
  page: number;
  searchQuery: string = "";

  // Access Child
  @ViewChild('productSlides') productSlides: Slides;

  constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public toastCtrl: ToastController,public modalCtrl: ModalController,public loadingCtrl: LoadingController) {

    this.page = 2;

    this.WooCommerce = this.WP.init();

    this.loadMoreProducts(null);

    this.WooCommerce.getAsync("products").then( (data) => {
      // let loading = this.loadingCtrl.create({
      //   spinner: 'hide',
      //   content: `
      //   <img src="../assets/images/icon.png"><h2 center>Loading</h2>`,
      //   });

      // loading.present();

      // setTimeout(() => {
      //   loading.dismiss();
      // }, 3000);
    
      this.products = JSON.parse(data.body).products;
      // console.log(data.body);
     console.log(JSON.parse(data.body));
    }, (err) => {
      console.log(err);
    });

  }

  ionViewDidLoad(){
    setInterval(()=> {
      if(this.productSlides.getActiveIndex() == this.productSlides.length() -1)
        this.productSlides.slideTo(0);
        this.productSlides.slideNext();
    }, 5000)
  }

  loadMoreProducts(event){
    console.log(event); 
    if(event == null)
    {
      this.page = 2;
      this.moreProducts = [];
    }
    else
      this.page++;

    this.WooCommerce.getAsync("products?page=" + this.page).then( (data) => {
      this.moreProducts = this.moreProducts.concat(JSON.parse(data.body).products);
      console.log(JSON.parse(data.body));
     
     
      if(event != null){
        event.complete();
     }

     if((JSON.parse(data.body).products.length < 10)){
       event.enable(false);

       this.toastCtrl.create({
         message: "No more products!",
         duration: 5000
       }).present();
     }


    }, (err) => {
      console.log(err);
    });
  }

  openProductPage(product){
    this.navCtrl.push('ProductDetailsPage', {"product": product});
  }

  backToHomepage(){
    this.navCtrl.setRoot('HomePage');
  }

  openCart(){
    this.modalCtrl.create(CartPage).present();
  }


  openSearchPage(){
    this.navCtrl.push('SearchPage', {"searchQuery": this.searchQuery})
  }

}

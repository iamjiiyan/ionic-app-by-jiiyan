import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import {WoocommerceProvider} from '../../providers/woocommerce/woocommerce';

@IonicPage({})
@Component({
  selector: 'page-user-order-details',
  templateUrl: 'user-order-details.html',
})
export class UserOrderDetailsPage {


  products: any[];
  WooCommerce: any;
  userOrder: any;
  product: any;

  constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {

    
    this.WooCommerce = this.WP.init();

    let loading = this.loadingCtrl.create({
      content: `Loading orders...`,
    });

    loading.present();


    this.userOrder = this.navParams.data.order;

    this.products = this.userOrder.line_items;

    this.products.forEach(element => {
      let productId = element.product_id;
      this.products = [];
      this.WooCommerce.getAsync("products/"+productId).then( (data) => {
       this.product = JSON.parse(data.body);
       this.product.product['order'] = element;
       this.products.push(this.product.product);
       console.log(this.products);
      });       
    });
    loading.dismiss();

    // console.log(this.products);
//  console.log(this.products);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserOrderDetailsPage');
  }

  openProductPage(product){
    this.navCtrl.push('ProductDetailsPage', {"product": product});
  }


}

import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { timer } from 'rxjs/observable/timer';
// import { MenuPage } from '../pages/menu/menu';

// import { SignupPage } from '../pages/signup/signup';
import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'MenuPage';

  showSplash = true;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public oneSignal: OneSignal) {
    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3000).subscribe(()=> this.showSplash = false);

      if (this.platform.is('cordova')) {
        // You're on a device, call the native plugins. Example: 
        //
        // var url: string = '';
        // 
        // Camera.getPicture().then((fileUri) => url = fileUri);
          this.oneSignal.startInit('dd82430c-400a-494d-ae27-af54e4e2fe66', '126955387773');
  
          this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
          
          this.oneSignal.handleNotificationReceived().subscribe(() => {
            // do something when notification is received
          });
          
          this.oneSignal.handleNotificationOpened().subscribe(() => {
            // do something when a notification is opened
          });
          
          this.oneSignal.endInit();
      } else {
        // You're testing in browser, do nothing or mock the plugins' behaviour.
        //
        // var
      }

    });
  }

}

import { Component } from '@angular/core';
import { IonicPage,  NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
@IonicPage({})
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {
  
  wpPostType: any;
  wpPosts : any[] = [];
  wpPost : any[] = [];
  wpUrl : string = "https://ido-ido.co/mangbenta/";
  featured_image: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http) {
    
    this.wpPostType = this.navParams.data.postType;
    
    console.log(this.navParams);
    let apiUrl: any;

    // if(this.wpPostType == "posts"){
      apiUrl = this.wpUrl + "wp-json/wp/v2/"+this.wpPostType
    // }else {
    //   apiUrl = this.wpUrl + "wp-json/wp/v2/"+this.wpPostType
    // }


    this.http.get(apiUrl).subscribe(res => {
      let temp: any[] = res.json();
      temp.forEach((element,index) => {
        let mediaRequest = element._links['wp:featuredmedia'][0].href;  
        // console.log(mediaRequest);
        this.http.get(mediaRequest).subscribe(data => {
          let  resdata = data.json();
          console.log(resdata);
        let imageThumb = resdata.media_details.sizes.thumbnail.source_url;
        // this.wpPost.push({'featured_image':imageThumb});
        element['featured_image'] = imageThumb;
        // console.log(imageThumb);
        })
        this.wpPosts.push(element);
      });
      
      
       
    });



  }
  


  // get_post_thumbnail_url(id){
  //   return new Promise((resolve) => {

  //   });
  // }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostPage');
  }

}

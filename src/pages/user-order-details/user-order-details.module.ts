import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserOrderDetailsPage } from './user-order-details';

@NgModule({
  declarations: [
    UserOrderDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserOrderDetailsPage),
  ],
  exports:[
    UserOrderDetailsPage
  ]
})
export class UserOrderDetailsPageModule {}


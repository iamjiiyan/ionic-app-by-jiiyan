import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
@IonicPage({})
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  WooCommerce: any;
  newOrder: any = [];
  paymentMethods: any[];
  paymentMethod: any;
  billing_shipping_same: boolean;
  userInfo: any;

  constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public alertCtrl: AlertController, public payPal: PayPal, public loadingCtrl: LoadingController) {
    
    this.newOrder = {};
    this.newOrder.billing = {};
    this.newOrder.shipping = {};
    this.billing_shipping_same = false;

    this.paymentMethods = [
      { method_id: "bacs", method_title: "Direct Bank Transfer" },
      { method_id: "cheque", method_title: "Cheque Payment" },
      { method_id: "cod", method_title: "Cash on Delivery" },
      { method_id: "paypal", method_title: "PayPal" }];

    this.WooCommerce = this.WP.init(true);

    let loading = this.loadingCtrl.create({
    content: `Please wait while getting your information...`,
    });

  loading.present();

 
        
    this.storage.get("userLoginInfo").then((userLoginInfo) => {

      this.userInfo = userLoginInfo.user;

      // let email = userLoginInfo.user.email;
      let id = userLoginInfo.user.id;

      this.WooCommerce.getAsync("customers/"+id).then((data) => {

        this.newOrder = JSON.parse(data.body);
        loading.dismiss();
        // console.log(this.newOrder);

      })

    })


  }



  setBillingToShipping(){
    this.billing_shipping_same = !this.billing_shipping_same;

    if(this.billing_shipping_same){
      this.newOrder.shipping_address = this.newOrder.billing_address;
    }
  }

  placeOrder(){
    
    let orderItems: any[] = [];
    let data: any = {};

    let paymentData: any = {};

    this.paymentMethods.forEach((element, index) => {
      if (element.method_id == this.paymentMethod) {
        paymentData = element;
      }
    });



    data = {

      //Fixed a bug here. Updated in accordance with wc/v2 API
      payment_method: paymentData.method_id,
      payment_method_title: paymentData.method_title,
      set_paid: true,

      billing: this.newOrder.billing,
      shipping: this.newOrder.shipping,
      customer_id: this.userInfo.id || '',
      line_items: orderItems
    };

    data.billing['email'] = this.userInfo.email;


      console.log(data);

    if (paymentData.method_id == "paypal") {

      this.payPal
        .init({
          PayPalEnvironmentProduction: "YOUR_PRODUCTION_CLIENT_ID",
          PayPalEnvironmentSandbox:
            "AfXCx0oxjD-QVjbon-eCszdAUZK_0sbNhlDhYsUn64sex4O83e_AnFVzTyz1R1e5wYchd-WSNLUwXB1i"
        })
        .then(
          () => {
            // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
            this.payPal
              .prepareToRender(
                "PayPalEnvironmentSandbox",
                new PayPalConfiguration({
                  // Only needed if you get an "Internal Service Error" after PayPal login!
                  //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
                })
              )
              .then(
                () => {
                  let loading = this.loadingCtrl.create({
                    // spinner: 'hide',
                    content: `Processing payment...`
                  });

                  loading.present();

                  this.storage.get("cart").then(
                    cart => {
                      let total = 0.0;
                      cart.forEach((element, index) => {
                        if (element.variation) {
                          orderItems.push({
                            product_id: element.product.id,
                            variation_id: element.variation.id,
                            quantity: element.qty
                          });
                          total = total + element.variation.price * element.qty;
                        } else {
                          orderItems.push({
                            product_id: element.product.id,
                            quantity: element.qty
                          });
                          total = total + element.product.price * element.qty;
                        }
                      });

                      let payment = new PayPalPayment(
                        total.toString(),
                        "PHP",
                        "Description",
                        "sale"
                      );
                      this.payPal
                        .renderSinglePaymentUI(payment)
                        .then(response => {
                          // Successfully paid
                          // alert(JSON.stringify(response));

                          data.line_items = orderItems;
                          //console.log(data);
                          let orderData: any = {};

                          orderData.order = data;

                          this.WooCommerce.postAsync(
                            "orders",
                            orderData.order
                          ).then(data => {
                            // alert("Order placed successfully!");

                            let response = JSON.parse(data.body);
                            loading.dismiss();
                            if (!response.errors) {
                              this.alertCtrl
                                .create({
                                  title: "Order Placed Successfully",
                                  message:
                                    "Your order has been placed successfully. Your order number is " +
                                    response.number,
                                  buttons: [
                                    {
                                      text: "OK",
                                      handler: () => {
                                        this.navCtrl.push("HomePage");
                                      }
                                    }
                                  ]
                                })
                                .present();
                            } else {
                              this.alertCtrl
                                .create({
                                  title: "Error!",
                                  message: response.errors[0].message,
                                  buttons: [
                                    {
                                      text: "OK",
                                      handler: () => {}
                                    }
                                  ]
                                })
                                .present();
                            }
                          });
                        });
                    },
                    () => {
                      // Error or render dialog closed without being successful
                    }
                  );
                },
                () => {
                  // Error in configuration
                }
              );
          },
          () => {
            // Error in initialization, maybe PayPal isn't supported or something else
          }
        );





    } else {

      if(paymentData.method_id == null){
        this.alertCtrl.create({
          title: "ERROR!",
          message: "Please Select Payment Method",
          buttons: [{
            text: "OK",
            handler: () => {
              // this.navCtrl.setRoot('CheckoutPage');
            }
          }]
        }).present();
      }else{

      let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        content: `Processing payment...`,
        });

      loading.present();
      
      this.storage.get("cart").then((cart) => {

        cart.forEach((element, index) => {
          if(element.variation){
            orderItems.push({ product_id: element.product.id, variation_id: element.variation.id, quantity: element.qty });
            ///total = total + (element.variation.price * element.qty);
          } else {
            orderItems.push({ product_id: element.product.id, quantity: element.qty });
            ///total = total + (element.product.price * element.qty);
          }
        });

        data.line_items = orderItems;

        let orderData: any = {};

        orderData.order = data;



        this.WooCommerce.postAsync("orders", orderData.order).then((data) => {
        //  console.log(JSON.parse(data.body));
          let response = (JSON.parse(data.body));
          loading.dismiss();
            this.alertCtrl.create({
              title: "Order Placed Successfully",
              message: "Your order has been placed successfully. Your order number is " + response.number,
              buttons: [{
                text: "OK",
                handler: () => {
                  this.navCtrl.setRoot('HomePage');
                }
              }]
            }).present();

          },(err)=>{
                console.log(err);
                  this.alertCtrl.create({
                  title: "Error while placing order.",
                  message: err.message,
                  buttons: [{
                    text: "OK",
                  }]
                }).present();
              })
          })
        console.log(this.newOrder);
      }
    }

    // AV6LbHCc7TPi8Oyc-wmSyCicUn_vcQyeqzbApJoguq8K-YtunoX73PGNoRt4VzEHm_hCttJUwZ1aQw34

    // console.log(this.newOrder);

  }


  
 
}
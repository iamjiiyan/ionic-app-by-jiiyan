import { Component } from '@angular/core';
import {  IonicPage, NavController, NavParams, ToastController, ModalController, LoadingController } from 'ionic-angular';
// import  * as WC from 'woocommerce-api';
// import { ProductDetailsPage } from '../product-details/product-details';
// import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import {WoocommerceProvider} from '../../providers/woocommerce/woocommerce'

@IonicPage({})

@Component({
  selector: 'page-products-by-category',
  templateUrl: 'products-by-category.html',
})
export class ProductsByCategoryPage {

    WooCommerce: any;
    products: any[];
    page: number;
    category: any;
    searchQuery: string = "";
  
    constructor(private WP: WoocommerceProvider, public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController,public modalCtrl: ModalController,public loadingCtrl: LoadingController) {
  
      this.page = 1;
      this.category = this.navParams.get("category");
                
        this.WooCommerce = this.WP.init();

        // Filtering Category
        this.WooCommerce.getAsync("products?filter[category]=" + this.category.slug).then( (data) => {
          this.products = JSON.parse(data.body).products;
          // console.log(data.body);
         console.log(JSON.parse(data.body));
        }, (err) => {
          console.log(err);
        });
  }
    
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductByCategoryPage');
  }

  
  loadMoreProducts(event){
    console.log(event); 

    this.page++;

    console.log("Getting page " + this.page);

    this.WooCommerce.getAsync("products?filter[category]=" + this.category.slug + "&page=" + this.page).then( (data) => {
      // let temp = (JSON.parse(data.body).products);
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `Loading products...`,
        });

      loading.present();

      this.products = this.products.concat(JSON.parse(data.body).products)

      console.log(this.products);
     
      event.complete();
      loading.dismiss();
      
      if((JSON.parse(data.body).products.length < 10)){
        event.enable(false);
 
        this.toastCtrl.create({
          message: "No more products!",
          duration: 5000
        }).present();
      }
       


    });
  }

  openProductPage(product){
    this.navCtrl.push('ProductDetailsPage', {"product": product});
  }

  backToHomepage(){
    this.navCtrl.setRoot('HomePage');
  }

  openSearchPage(){
    this.navCtrl.push('SearchPage', {"searchQuery": this.searchQuery})
  }

  openCart(){
    this.modalCtrl.create(CartPage).present();
  }

}

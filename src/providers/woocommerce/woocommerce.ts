// import { HttpClient } from '@angular/common/http';/
import { Injectable } from '@angular/core';
import  * as WC from 'woocommerce-api';

@Injectable()
export class WoocommerceProvider {

  WooCommerce: any;
  WoocommerceV2: any;

  constructor() {

    this.WooCommerce = WC({
      // This is Require to get data from woocommerces
      // url: "https://ido-ido.co/mangbenta",
      // consumerKey: "ck_80e53d5753dc9beea00445b174a66d625f3a375e",
      // consumerSecret: "cs_861f550cea5ec6a9da2f073018a872f943c70d3f",
      url: "https://hmcrafts.com.au",
      consumerKey: "ck_ce5ef21a734c3914ee8bacfed048d3b2a8331fe4",
      consumerSecret: "cs_1a6d2a15e6fd6d77467d2385895e1dbfcb5163f2",
    });

    this.WoocommerceV2 = WC({
      url: "https://hmcrafts.com.au",
      consumerKey: "ck_ce5ef21a734c3914ee8bacfed048d3b2a8331fe4",
      consumerSecret: "cs_1a6d2a15e6fd6d77467d2385895e1dbfcb5163f2",
      wpAPI: true,
      version: "wc/v2"
    });

  }

  init(v2?: boolean){
    if(v2 == true){
      return this.WoocommerceV2;
    } else {
      return this.WooCommerce;
    }
  }

}

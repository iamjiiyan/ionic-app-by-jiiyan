import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
// import { SignupPage } from '../signup/signup';
@IonicPage({

})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string;
  password: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public toastCtrl: ToastController, public storage: Storage, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
     
    this.username = "";
    this.password = "";
   
  }

  login(){
    // ?insecure=cool remove when https
    let loading = this.loadingCtrl.create({
      content: `Verifying Account...`,
    });

    loading.present();
    
    this.http.get("https://hmcrafts.com.au/api/auth/generate_auth_cookie/?insecure=cool&username=" + this.username + "&password=" + this.password)
    .subscribe( (res)=> {
      console.log(res.json());

      let response = res.json();

      if(response.error){
        this.toastCtrl.create({
            message: response.error,
            duration:5000
        }).present();
        loading.dismiss();
        return;
      }
      loading.dismiss();
      this.storage.set("userLoginInfo", response).then( (data) =>{

        this.alertCtrl.create({
          title: "Login Sucessful",
          message: "You have been logged in successfully.",
          buttons: [{
            text: "OK",
            handler: () => {
              if(this.navParams.get("next")){
                this.navCtrl.push(this.navParams.get("next"));
              }else{
                this.navCtrl.setRoot('MenuPage');
              }
            }
          }]
        }).present();
        
      });

    });
  }

  openPage(pageName:string){
    if(pageName == 'signup'){
      this.navCtrl.push('SignupPage');
    }
  }
}
